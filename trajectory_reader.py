import os
import pandas as pd
import pickle
import cszml_managment as cszml
from datetime import datetime, timedelta

id_key='ID'
time_key='time vector'
prop_key='propagations'

def dataframe_reader(path):
        a_file = open(os.path.join(path), "rb")
        output = pickle.load(a_file)
        a_file.close()
        df_propagations = pd.DataFrame.from_dict(output)
        
        return df_propagations

def reduce_points_number(prop_list,time_list,timestep):
    timedelta_step=timedelta(seconds=timestep)
    epoch=datetime.strptime(time_list[0],'%d/%m/%Y %H:%M:%S.%f')
    epoch_str=epoch.strftime('%Y%m%dT%H%M%SZ')
    num_el=len(time_list)
    
    reduced_time_pos_list=[]
    date_prev=epoch
    
    reduced_time_pos_list.append(0)
    reduced_time_pos_list.append(prop_list[0][0])
    reduced_time_pos_list.append(prop_list[0][1])
    reduced_time_pos_list.append(prop_list[0][2])
    
    for i in range(num_el):
        date=datetime.strptime(time_list[i],'%d/%m/%Y %H:%M:%S.%f')
        if (date-date_prev)>=timedelta_step:
            seconds=date-epoch
            seconds=seconds.days*86400+seconds.seconds+seconds.microseconds/1e6
            date_prev=date
            reduced_time_pos_list.append(seconds)
            reduced_time_pos_list.append(prop_list[i][0])
            reduced_time_pos_list.append(prop_list[i][1])
            reduced_time_pos_list.append(prop_list[i][2])
        
    
    return epoch_str,reduced_time_pos_list

def propagations_to_dict(prop_df):
    prop_total=[]
    counter_tot = len(prop_df)-1
    for index,row in prop_df.iterrows():
        prop_row_dict={}
        prop_row_dict['id']=row[id_key]
        
        epoch_str,reduced_time_pos_list=reduce_points_number(row[prop_key],row[time_key],timestep=300)
        prop_row_dict['epoch']=epoch_str
        prop_row_dict['cartesian']=reduced_time_pos_list
        prop_total.append(prop_row_dict)
        
        # if index==5:
        #     break

        if(index%round(counter_tot*0.05)):
            print(index/counter_tot*100)
        
    return prop_total
        


if __name__=='__main__':
    path="/home/ubuntu/catalogs_NISIDE/propagations.pkl"
    
    prop=dataframe_reader(path)
    prop_total_list=propagations_to_dict(prop)
    cszml.from_data_to_czml(prop_total_list)
    a=1
    pass

'''

       "interpolationAlgorithm": "LAGRANGE",
            "interpolationDegree": 5,
            "referenceFrame": "INERTIAL",
            "epoch": "20140715T130255Z",
            "cartesian": [
                0,
                -4523158.946108278,
                4974755.3926670505,
                973796.9219644082,
                300,
                -5483598.473258102,
                3927485.6504392726,
                -819481.5189799711,
                600,
                -5822099.923416168,
                2434745.8688244275,
                -2519573.1542970072,
                900,
                -5500958.93829691,
                666108.8794233681,
                -3933426.426193799,
                1200,
                -4557448.977191549,
                -1177939.650874888,
                -4901041.271107068,
                1500,
                -3098994.8713867627,
                -2888937.858212128,
                -5313358.452854597,
                1800,
                -1290649.589421391,
                -4273782.676854467,
                -5124088.224063983,
                2100,
                663410.8341995103,
                -5176047.322256854,
                -4354598.270790426,
                2400,
                2542457.57322875,
                -5493341.630953689,
                -3091609.989793312,
                2700,
                4133775.7529580663,
                -5189045.694134789,
                -1477839.6869088674,
                3000,
                5256798.781596554,
                -4296946.519933884,
                303782.83779966214,
                3300,
                5783988.569934239,
                -2917815.8451058124,
                2050865.8454086138,
                3600,
                5655669.575847693,
                -1207990.387488192,
                3564872.9572488293
            ]
        }
        
        
'''