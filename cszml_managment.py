import json

def create_an_orbit_object(id,name,epoch_time,coordinates_list,color_label =[0,255,0,255],color_point = [0,255,0,255],color_orbit = [0,255,0,255]):
    output = {
                "id": str(id),
                "name": str(name),
                "label": {
                "text": str(name),
                "scale": 0.5,
                "eyeOffset": {
                    "cartesian": [
                        1000,
                        1000,
                        0
                    ]
                },
                "fillColor": {
                    "rgba": color_label
                }
            },
            "path": {
                "width": 2,
                "leadTime": 0,
                "trailTime": 0,
                "material": {
                    "polylineOutline": {
                        "color": {
                            "rgba": color_orbit
                        }
                    }
                }
            },
            "point": {
                "pixelSize": 2,
                "color": {
                    "rgba": color_point
                },
                "outlineColor": {
                    "rgba": [
                        255,
                        255,
                        255,
                        255
                    ]
                },
                "outlineWidth": 1
            },
            "position": {
                "interpolationAlgorithm": "LAGRANGE",
                "interpolationDegree": 5,
                "referenceFrame": "INERTIAL",
                "epoch": str(epoch_time),
                "cartesian": coordinates_list
            }
        }
    return output

def from_data_to_czml(input_data):
    data_set = [
        {
            "id": "document",
            "name": "SGP4 route",
            "description": "Route for a satellite object",
            "version": "1.0",
            "clock":       {
                "interval": "20090210T000000Z/20090211T000000Z",
                "currentTime": "20090210T000000Z"
            }
        },
    ]
    
    for elem in input_data:
        id = elem['id']
        epoch_time = elem['epoch']
        coordinates_list = elem['cartesian']
        data_set.append(create_an_orbit_object(id,"",epoch_time,coordinates_list))

      
    json_dump = json.dumps(data_set)

    with open('celsium_data.czml', 'w') as outfile:
        json.dump(data_set, outfile,indent=4)
    return json_dump

# input_data = [[1,"20140715T130255Z",[
#                 0,
#                 -4523158.946108278,
#                 4974755.3926670505,
#                 973796.9219644082,
#                 300,
#                 -5483598.473258102,
#                 3927485.6504392726,
#                 -819481.5189799711,
#                 600,
#                 -5822099.923416168,
#                 2434745.8688244275,
#                 -2519573.1542970072,
#                 900,
#                 -5500958.93829691,
#                 666108.8794233681,
#                 -3933426.426193799,
#                 1200,
#                 -4557448.977191549,
#                 -1177939.650874888,
#                 -4901041.271107068,
#                 1500,
#                 -3098994.8713867627,
#                 -2888937.858212128,
#                 -5313358.452854597,
#                 1800,
#                 -1290649.589421391,
#                 -4273782.676854467,
#                 -5124088.224063983,
#                 2100,
#                 663410.8341995103,
#                 -5176047.322256854,
#                 -4354598.270790426,
#                 2400,
#                 2542457.57322875,
#                 -5493341.630953689,
#                 -3091609.989793312,
#                 2700,
#                 4133775.7529580663,
#                 -5189045.694134789,
#                 -1477839.6869088674,
#                 3000,
#                 5256798.781596554,
#                 -4296946.519933884,
#                 303782.83779966214,
#                 3300,
#                 5783988.569934239,
#                 -2917815.8451058124,
#                 2050865.8454086138,
#                 3600,
#                 5655669.575847693,
#                 -1207990.387488192,
#                 3564872.9572488293
#             ]]]



# print(from_data_to_czml(input_data))